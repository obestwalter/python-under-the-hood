"""Find out more about the gravity cannon: https://docs.python.org/3/library/gc.html"""
import gc
import sys


def find_names(obj):
    frame = sys._getframe()  # https://docs.python.org/3/library/sys.html#sys._getframe
    for frame in iter(lambda: frame.f_back, None):
        frame.f_locals  # trigger locals dict creation (optimized away)

    result = set()
    for referrer in gc.get_referrers(obj):
        if isinstance(referrer, dict):
            for k, v in referrer.items():
                if v is obj:
                    result.add(k)

    return result


def demo():
    eggs = spam

    def inner():
        ham = spam
        print(find_names(ham))

    inner()


spam = [1, 2, 3]

demo()
