"""Modules are singletons according to pretty much everybody ... but not always.

see e.g.
https://python-patterns.guide/gang-of-four/singleton/
https://www.pythonmorsels.com/making-singletons/
https://stackoverflow.com/a/52930277/2626627
https://www.geeksforgeeks.org/singleton-pattern-in-python-a-complete-guide/
https://medium.com/geekculture/python-five-ways-to-write-singleton-b6afbed65680
https://dev.to/taikedz/do-you-need-singletons-in-python--582
etc.
"""
from modules.d_modules_are_not_singletons_optional import import_me as i1
print("Importing it again ... already imported ... cached")
from modules.d_modules_are_not_singletons_optional import import_me as i2

print("Importing it again, but via a different path ...")
import import_me as i3
print("That shouldn't have happened.\n")

i1.x = 1
print(f"{i1.x=} | {i2.x=}")
i1.x = 2
print(f"{i1.x=} | {i2.x=}\n")

try:
    print(f"{i3.x=}")
except Exception as e:
    print("Apparently modules are not always singletons.")
    print(f"{id(i3)=} vs {id(i2)=} | {i3 == i1=}")
