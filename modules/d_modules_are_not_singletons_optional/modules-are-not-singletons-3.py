"""Modules are safed in a global dictionary with the import path as name."""
import sys

from modules.d_modules_are_not_singletons_optional import import_me as i1
import import_me as i2

assert i1 is not i2

print(f"{i1.__name__=}, {id(i1)=}")
print(f"{i2.__name__=}, {id(i2)=}")

for name, module in sys.modules.items():
    if "import_me" not in name:
        continue

    print(f"{name=}, {id(module)=}")
