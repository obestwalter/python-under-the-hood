"""Can I import that module as if it where a 'real' module in the path?"""
import sys

from modules_1a import x

if __name__ == '__main__':
    from pprint import pprint

    pprint(sys.modules)  # nothing here - it's just an object when imported like this
    print(f"{x=}")
else:
    sys.modules["module_from_far_far_away"] = x
    del x
