"""Modules are objects, which also can be instantiated like any other instance."""
import sys

x = type(sys)("Module from far far away")    # you could also import ModuleType ...

if __name__ == '__main__':
    class_of_module = x.__class__
    print(f"{class_of_module is type(x)=}")
    print(f"{class_of_module.mro()=}")

    assert not hasattr(x, "__file__")

x.__file__ = "/a/fantasy/file/system/far/far/away.py"
x.some_function = lambda: print("A function in a module from far far away")
