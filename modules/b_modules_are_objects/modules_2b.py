"""Can I import that module as if it where a 'real' module in the path?"""
import modules_2a

assert not hasattr(modules_2a, "x")

import module_from_far_far_away as x

print(f"{x=}")
