"""Modules are objects, which are usually imported from a .py file."""
import sys

import import_me

x = import_me

if __name__ == '__main__':
    print(f"{type(x).mro()=}")
    print(f"{x.__file__=}")                # set if imported from the file system
    assert not hasattr(sys, "__file__")    # but there are modules which are not
