"""As modules are just objects - they can also have customizable special methods."""

import modules_4a

print(f"{ modules_4a.x()=}")
print(f"{ modules_4a.y()=}")
print(f"{ modules_4a.whatever=}")

try:
    modules_4a.something_else()
except AttributeError as e:
    print(f"Nope - {e}")
