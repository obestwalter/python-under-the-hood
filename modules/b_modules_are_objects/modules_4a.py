"""As modules are just objects - they can also have customizable special methods.

see https://peps.python.org/pep-0562/
"""
from random import random

def __getattr__(name):
    if name in ["x", "y"]:
        return lambda: random()

    if name == "whatever":
        return 1

    raise AttributeError(f"{__name__} has no attribute '{name}'")
