"""Find out more about the gravity cannon: https://docs.python.org/3/library/gc.html."""
import gc  # import the gravity cannon!
from types import ModuleType

from modules.c_gravity_cannon_1 import a_normal_module

a_normal_module.im_just_some_function()

module_fetched_with_gravity_cannon = [
    module for module in gc.get_objects()
    if isinstance(module, ModuleType)
       and module.__name__ == "a_secret_module"
][0]

print(f"{module_fetched_with_gravity_cannon.a_secret_function}")
module_fetched_with_gravity_cannon.a_secret_function()
