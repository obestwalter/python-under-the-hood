"""Importing a correctly named module the conventional way."""
from modules.a_importing import import_me as module

print(f"{module=}")
