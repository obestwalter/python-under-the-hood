"""This is not just a signal that a folder is a Python package.

This module also gets automatically imported as soon as anything in this package
gets imported.
"""
from importlib import util
from pathlib import Path
from types import ModuleType


def import_python_module(path: Path) -> ModuleType:
    """Given a path: load a Python module, evaluate and return the it."""
    spec = util.spec_from_file_location(path.stem, path)
    module = util.module_from_spec(spec)
    spec.loader.exec_module(module)
    return module
