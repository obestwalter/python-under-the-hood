"""Using importlib to import a weirdly named module."""
from pathlib import Path

from modules.a_importing import import_python_module

module = import_python_module(Path(__file__).parent / "666 weird-import-me.py")

print(f"{module=}")
# Note the different import name - no dotted path - just the module name
