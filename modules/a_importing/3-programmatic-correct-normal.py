"""Using importlib to import a module given the path to the file."""
from pathlib import Path

from modules.a_importing import import_python_module

module = import_python_module(Path(__file__).parent / "import_me.py")

print(f"{module=}")
# Note the different import name - no dotted path - just the module name
