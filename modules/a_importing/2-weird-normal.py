"""Using the expression implementation of the import statement to import a module."""
module = __import__("import_me")

print(f"{module=}")
