print(f"I am '{__name__}'", end=" - ")
if __name__ == '__main__':
    print(f"you executed me as a script.")
else:
    print("thanks for importing me!")
