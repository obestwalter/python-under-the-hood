"""Using the expression variant of the import statement to import a weird module."""
module = __import__("666 weird-import-me")

print(f"{module=}")
