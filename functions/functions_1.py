"""Functions are first class objects."""

def x():
    print("I'm a function")

print(f"{id(x)=}")                     # pass it to a function - e.g. to get the id
print(f"{type(x)=}")                   # has a type (custom function)
print(f"{type(dir)=}")                 # has a type (builtin function)
print(f"{type(x) is x.__class__=}")    # this is usually true (unless black magic)
print(f"{dir(x)=}")                    # it has lots of special attributes
print(f"{x.__dict__=}")                # it also has a dictionary
