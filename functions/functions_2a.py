"""The same using syntax sugar introduced in the early Python 2 days."""

def decorator(func):
    def x(*args, **kwargs):  # nested functions: side effect of first class objects
        print("I am a decorator ...")
        result = func(*args, **kwargs)
        print("... function.")
        return result
    return x

@decorator
def some_function():
    print("I am a function")
    return 1


if __name__ == '__main__':
    some_function()
    was_decorated = some_function # I can still use a new name if I want ...
    print(f"{some_function=} => {was_decorated()=}")
