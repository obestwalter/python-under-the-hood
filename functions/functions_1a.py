"""If functions have __dict__, can we do the same as with any other instance?"""

def x():
    print("I'm a function")
    return True

print(f"{x()=}")
print(f"{x.__dict__=}")
x.y = lambda: x() and print("The last function call was successful.") or 3
print(f"{x.__dict__=}\n")
print(f"{x.y()=}")
