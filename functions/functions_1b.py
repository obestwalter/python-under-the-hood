"""If functions have a type, can we inherit from that type and do custom functions?"""

def x(): pass

BuiltinA = type(dir)
BuiltinB = dir.__class__
CustomA = type(x)
CustomB = x.__class__

for func_type in [BuiltinA, BuiltinB, CustomA, CustomB]:
    try:
        class MyCustomFunctionType(func_type):
            """Tim Peters in the python mailing list sometime 2003:

            [Lenard Lindstrom]
            > Would someone please explain why subtyping of type 'function' is not
            > permitted in Python 2.3?

            Nothing implemented in C is subclassable unless somebody volunteers the work
            to make it subclassable; nobody volunteered the work to make the function
            type subclassable.  It sure wasn't at the top of my list <wink>.
            """
        print(f"{MyCustomFunctionType=}")
    except TypeError as e:
        print(f"Nope for {func_type=} - {e}")
