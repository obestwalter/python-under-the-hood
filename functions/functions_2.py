"""Decorators are a popular use of first class functions."""

def decorator(func):
    def x(*args, **kwargs):  # nested functions: side effect of first class objects
        print("I am a decorator ...")
        result = func(*args, **kwargs)
        print("... function.")
        return result
    return x


def some_function():
    print("I am a function")
    return 1


if __name__ == '__main__':
    some_function()
    # Nice way to shoot yourself in the foot:
    #   Now function is decorated when running module as script, but isn't if imported.
    some_function = decorator(some_function)
    print(f"{some_function=} => {some_function()=}")
