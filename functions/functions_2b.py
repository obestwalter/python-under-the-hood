"""In the second variant, the original function has 'vanished', can we recover it?"""
import inspect

from functions_2a import some_function

# This is the direct/brittle way to get to the original function:
original_raw = some_function.__closure__[0].cell_contents
original_raw()

# This is the correct way to get to the original function:
inspect_info = inspect.getclosurevars(some_function)
print(f"{inspect_info=}")
original_inspect = inspect_info.nonlocals["func"]
original_inspect()

assert original_raw is original_inspect
