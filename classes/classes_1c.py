"""Instances created from custom class objects can be mutated."""

x = type("x", tuple(), {})()   # instance of a custom class object

x.whatever = 1                 # can attach data dynamically
print(f"{x.whatever=}")

x.spam = lambda: 2             # can attach behaviour dynamically
print(f"{x.spam=}")
print(f"{x.spam()=}")

# There is really no conceptual difference between data and behaviour though
#  behaviour is just an attribute that happens to be callable
assert not callable(x.whatever)
assert callable(x.spam)
