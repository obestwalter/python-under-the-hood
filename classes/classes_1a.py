"""Instances of 'object' also can't be mutated."""

x = object()

try:
    x.whatever = 1
except AttributeError as e:
    print(f"Nope - {e}")

try:
    x.__dict__["whatever"] = 1
except AttributeError as e:
    print(f"Nope - {e}")
