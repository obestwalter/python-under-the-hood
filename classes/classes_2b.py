"""Protocols make it possible to change behaviour - attribute access."""
class A:
    def __init__(self, thing): self._thing = thing

    def ham(self): return self._thing * 1

    def eggs(self): return self._thing * 2

    def spam(self): return self._thing * 3

    def __getattribute__(self, name):
        """Always called on attribute access."""
        print(f"[IN __getattribute__] accessing {self!r}.{name}")
        return object.__getattribute__(self, name)

    def __getattr__(self, name):
        """Called, when normal attribute search fails."""
        import random
        from functools import partial

        print(f"[IN __getattr__] accessing {self!r}.{name}")

        if "surprise_me" in name:
            choices = [
                    value for key, value in self.__class__.__dict__.items()
                    if not key.startswith("_")
                ]
            print(f"Choosing from {choices}.")
            func = random.choice(choices)

            # more correct would likely be: https://stackoverflow.com/a/1015405/2626627
            bound_method_sim = partial(func, self)

            print(f"{func=} -> {bound_method_sim}")
            return bound_method_sim

        raise AttributeError(f"Dunno anything about {name}")


a = A("flowers")
print(f"{a.spam()=}\n")

print(f"{a.surprise_me_with_something()=}\n")

try:
    a.i_dont_exist
except AttributeError as e:
    print(f"Nope - {e}")
