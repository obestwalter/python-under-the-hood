"""For completeness: this is how you would usually write this class."""

class X:
    @staticmethod
    def sm_on_class():
        return 3

    def im_on_class(self):
        return f"{self=}, {type(self)=}"


print(f"{X.sm_on_class()=}")      # calling a static method on the class object
print(f"{X().sm_on_class()=}")    # ... and on the instance object

try:
    print(f"{X.im_on_class()=}")  # calling an instance method on the class
except TypeError as e:
    print(f"Nope - {e}")
    print(f"Can be worked around though: {X.im_on_class(1)}")

print(f"{X().im_on_class()=}")    # calling an instance method the right way
