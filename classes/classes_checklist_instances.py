"""Checklist: instances are first class objects."""

a = type("A", tuple(), {})()
b = type("B", tuple(), {})()

def a_generic_function(obj):
    obj.new_thing = 1
    return obj

# [X] can be the subject of assignment statements
# [X] can be the actual parameters of functions
# [X] can be returned as results of functions
x = a_generic_function(a)
print(f"{x.new_thing=}")

# [X] test for equality
print(f"{a == b=}")  # different instances are not equal by default
