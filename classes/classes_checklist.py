"""Checklist: classes are first class objects."""

class A: ...
class B: ...

def a_generic_function(obj):
    obj.new_thing = 1
    return obj

# [X] can be the subject of assignment statements
# [X] can be the actual parameters of functions
# [X] can be returned as results of functions
x = a_generic_function(A)
print(f"{x.new_thing=}")

# [X] test for equality
print(f"{A == B=}")  # different class objects are not equal by default
