"""The fundamental difference between builtin and custom classes is mutability."""

x = object

try:
    x.whatever = 1
except TypeError as e:
    print(f"Nope - {e}")

try:
    x.__dict__["whatever"] = 1
except TypeError as e:
    print(f"Nope - {e}")
