"""Protocols make it possible to change behaviour - e.g. instance creation."""

class A:  # e.g. instance creation
    def __new__(cls):
        return 1  # Why would you do that? You wouldn't - but mechanically it's possible

a1 = A()
a2 = A()
a3 = A()

print(f"{a1=}, {a2=}, {a3=}")
print(f"{a1 == a2 == a3}")
print(f"{a1 is a2 is a3}")

print(f"{id(1)=}, {id(a1)=}")

print(f"{1 .__class__=}, {a1.__class__=}")  # objects of this class 'A' or of class 'int'
#         ^ notice the space - needed to avoid ambivalence with float in parser
