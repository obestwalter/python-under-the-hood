"""Everything is a first class citizen in Python - even the basic type."""

x = object                 # since python2.2 'object' is the mother of all classes

print(f"{id(x)=}")         # has an id (memory address in CPython)
print(f"{type(x)=}")       # has a type (here 'type' itself - the default metatype)
print(f"{x is x=}")        # can be checked for identity
print(f"{x == x=}")        # can be checked for equivalence

print(f"{dir(x)=}")        # has (special) attributes that will be inherited

print(f"{x.__dict__=}")    # has a dictionary for all attributes (data and behaviour)
print(f"{x.mro()=}")       # has a method resolution order
