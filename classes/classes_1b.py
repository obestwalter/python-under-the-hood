"""Custom class objects can be mutated."""

class x: pass

try:
    x.whatever = 1
except TypeError as e:
    print(f"Nope - {e}")
else:
    print(f"Setting class attributes works here - {x.whatever=}")

try:
    x.__dict__["whatever"] = 1   # same as above, but forbidden on class level
except TypeError as e:
    print(f"Nope - {e}")
