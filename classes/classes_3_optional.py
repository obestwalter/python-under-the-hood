"""Behaviour of class instances can be changed dynamically, but it's tricky."""

class X: pass

x = X()

x.__dict__["on_instance"] = lambda: 2  # attach non-instance function to instance
print(f"{x.__dict__}")
print(f"{x.on_instance()}")  # behaves like staticmethod but lives only on instance
y = X()                      # creating another instance shows this
print(f"{y.__dict__}")       # nothing here
y.on_instance()              # really - it doesn't work

x.__class__.__dict__["on_class"] = lambda: 3  # can't be mutated directly

setattr(x.__class__, "on_class", lambda: 3)   # but indirectly
x.__class__.on_class = lambda: 3              # same as above
print(f"{x.__class__.__dict__}")
print(f"{x.on_class()}")                      # very broken - gets self passed

X.sm_on_class = staticmethod(lambda: 3)       # either make it static
print(f"{x.__class__.__dict__}")
print(f"{x.sm_on_class()}")

X.im_on_class = lambda _: 4                   # or make accept a positional argument
print(f"{x.__class__.__dict__}")
print(f"{x.im_on_class()}")
