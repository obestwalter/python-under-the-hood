"""Protocols make it possible to change behaviour - e.g. comparisons."""

class A: ...

class B:
    def __eq__(*_):
        return True

C = type("B", (object,), {"__eq__": lambda *_: True})

class D: (__eq__:= lambda *_: True)  # wow ... ok - that also works :O

print(f"{A() == A()=}")  # instances same/different classes are not equal by default
print(f"{A() == B()=}")  # so normally these should all be false ...
print(f"{A() == C()=}")
print(f"{B() == C()=}")
print(f"{C() == A()=}")
print(f"{C() == B()=}")
print(f"{D() == D()=}")
