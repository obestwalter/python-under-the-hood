"""Custom class instances also look and behave basically the same as object()."""
class X: pass
x=X()                      # instance of a custom class object
print(f"{id(x)=}")         # has an id (memory address in CPython)
print(f"{type(x)=}")       # has a type (here type itself - the default metatype)
print(f"{x is x=}")        # can be checked for identity
print(f"{x == x=}")        # can be checked for equivalence

print(f"{dir(x)=}")        # has inherited attributes and could have own attributes

print(f"{x.__dict__=}")    # has a dictionary for all attributes (data and behaviour)
# print(f"{x.mro()=}")     # has no method resolution order - only on class objects
