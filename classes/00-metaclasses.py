"""First class objects and the meta object protocol in one little demo.

Visualize the path the Python interpreter takes for these steps:
* write a metaclass
* write a class using that metaclass instead of the default metaclass type
* create an instance of that class
* call the instance

At the end we also print the class hierarch for that instance.

An analogy for all this could be:
    * if an instance is a cookie, then
    * a class is a cookiecutter and
    * a metaclass is the machine that can make the cookiecutters

Decluttered version in pythontutor: https://tinyurl.com/metaclasses-and-protocols
"""
sprint = lambda *args, **kwargs: print("#" * 23, *args, "#" * 23, **kwargs)
sprint(f"[MODULE NAMESPACE] before creation of metaclass object")

class CustomMeta(type):  # metaclass/metatype
    """This does the same as type - only more chatty."""

    print(f"[BEGIN {__qualname__} NAMESPACE]\n{locals()=}\n")

    def __new__(mcs, name, bases, dict_):
        print(f"[IN METACLASS __new__] {mcs=}, {name=}, {bases=}, {dict_=}\n")
        cls = super().__new__(mcs, name, bases, dict_)  # call into type functionality
        return cls

    def __init__(cls, name, bases, dict_):
        print(f"[IN METACLASS __init__] {cls=}, {name=}, {bases=}, {dict_=}\n")
        super().__init__(name, bases, dict_)

    def __call__(cls, *args):
        print(f"[IN METACLASS __call__] {cls=}, {args=}\n")
        obj = super().__call__(*args)
        return obj

    print(f"[END {__qualname__} NAMESPACE]\n{locals()=}\n")

sprint(f"[MODULE NAMESPACE] after creation of metaclass object")
sprint(f"[MODULE NAMESPACE] before creation of class object")

class ClassWithCustomMeta(object, metaclass=CustomMeta):  # class/type

    print(f"[BEGIN {__qualname__} NAMESPACE]\n{locals()=}\n")

    def __new__(cls, *args, **kwargs):
        print(f"[IN CLASS __new__] {cls}.__new__\n")
        return super().__new__(cls, *args, **kwargs)

    def __init__(self):
        print(f"[IN CLASS __init__] {self}.__init__\n")

    def __call__(self):
        print(f"[IN CLASS __call__] {self}.__call__\n")
        return 42

    print(f"[END {__qualname__} NAMESPACE]\n{locals()=}\n")

sprint(f"[MODULE NAMESPACE] after creation of class object")

sprint(f"[MODULE NAMESPACE] before creation of instance")

x = ClassWithCustomMeta()  # instance
sprint(f"[MODULE NAMESPACE] after creation of instance")

sprint(f"[MODULE NAMESPACE] before calling the instance")
# As we implemented __call__ on a 'normal' class, the instance is also callable.
print(f"{x()=}")
sprint(f"[MODULE NAMESPACE] after calling the instance")

sprint(f"[MODULE NAMESPACE] type hierarchy of the instance")
print(f"{x=}")
print(f"{type(x)=}")
print(f"{type(type(x))=}")
print(f"{type(type(type(x)))=}")
print(f"{type(type(type(type(x))))=}")
