"""An instance of the basic type also looks very similar ..."""

x = object()               # class instance

print(f"{id(x)=}")         # has an id (memory address in CPython)
print(f"{type(x)=}")       # has a type (here 'object' - the default instance type)
print(f"{x is x=}")        # can be checked for identity
print(f"{x == x=}")        # can be checked for equivalence

print(f"{dir(x)=}")        # has no attributes by itself - these are all from the class

# print(f"{x.__dict__=}")  # has no dictionary (ergo no 'own' attributes)
# print(f"{x.mro()=}")     # has no method resolution order - only on class objects
