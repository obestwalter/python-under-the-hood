# Materials from an internal talk

This is supposed to be a quick introduction into first class objects and the meta object protocol, which are the two fundamental features of Python that make a lot of really interesting libraries possible and can help to understand better what is going on, when using these libraries or writing your own.

You can run the code and explore it, in Python3.8 or greater. The code only uses builtins and stdlib, so no venv needed.

# Further reading material

* [Unifying types and classes in Python 2.2](https://www.python.org/download/releases/2.2.3/descrintro/)
* [Metaclasses in Python 1.5](https://www.python.org/doc/essays/metaclasses/)
* [Python data model](https://docs.python.org/3/reference/datamodel.html) 
* [Python low level object protocol](https://docs.python.org/3/c-api/object.html)
* [Python import system](https://docs.python.org/3/reference/import.html)
* [Why your mock doesn't work](https://nedbatchelder.com/blog/201908/why_your_mock_doesnt_work.html)

# Copyright notice

<a rel="license" href="http://creativecommons.org/licenses/by-nc/4.0/"><img alt="Creative Commons Licence" style="border-width:0" src="https://i.creativecommons.org/l/by-nc/4.0/88x31.png" /></a><br />This work is licensed under a <a rel="license" href="http://creativecommons.org/licenses/by-nc/4.0/">Creative Commons Attribution-NonCommercial 4.0 International License</a>.
